var sections = document.getElementsByClassName("section");

function toggleSection(element) {
    element.classList.toggle("active");
    var content = element.nextElementSibling;
    if (content.style.maxHeight){
        content.style.maxHeight = null;
      } else {
        content.style.maxHeight = content.scrollHeight + "px";
      } 
}

toggleSection(sections[0]);
for (var i = 0 ; i < sections.length; i++) {
    sections[i].addEventListener("click", function() {
        toggleSection(this);
    })
}